package com.minderaschool.explorer.agent.brains;

import com.minderaschool.explorer.client.BeaconMeasure;
import com.minderaschool.explorer.client.Robot;

public class FinalBrain {
    public Robot robot;
    public String robName;
    public double centerSensor, leftSensor, rightSensor, compass;
    private BeaconMeasure beacon;
    private int ground;
    private FinalBrain.State state;
    private String lastRotate;
    private double currentTime, startRotateTime = 0;
    private int countRot = 0;

    private double Time_To_Rotate = 6.0;
    // este é o tempo que ele demora a rodar 90graus com uma velocidade de +0.131 , -0.131

    private enum State {MOVE_AVOID, FOUND_CHEESE, GO_BEACON, ROTATE_RIGHT, ROTATE_LEFT, GO_HOME}

    public FinalBrain(String robName, int pos, String host) {
        robot = new Robot();
        beacon = new BeaconMeasure();
        ground = -1;
        state = State.MOVE_AVOID;

        this.robName = robName;

        // register robot in simulator
        robot.initRobot(robName, pos, host);
    }

    public void mainLoop() {

        while (true) {
            robot.readSensors();
            getSensors();
            decide();
            //ver valores dos sensores
            //System.out.println("Beacon: " + beacon.beaconVisible + " " + beacon.beaconDir);
            //System.out.println("lefSensor: " + leftSensor);
            //System.out.println("rightSensor: " + rightSensor);
            //System.out.println("centerSensor: " + centerSensor);

        }
    }

    public void getSensors() {

        currentTime = robot.getTime();

        centerSensor = robot.getObstacleSensor(0);

        leftSensor = robot.getObstacleSensor(1);

        rightSensor = robot.getObstacleSensor(2);

        compass = robot.getCompassSensor();

        ground = robot.getGroundSensor();

        beacon = robot.getBeaconSensor(0);


    }

    private void stop() {
        robot.driveMotors(0, 0);
    }

    public void decide() {

        switch (state) {
            case MOVE_AVOID:    /* Go */
                //System.out.println("MOVE_AVOID");
                if (centerSensor > 1.2) {
                    if (rightSensor > leftSensor && rightSensor > 1.2) {
                        state = State.ROTATE_LEFT;
                    } else if (leftSensor > rightSensor && leftSensor > 1.2) {
                        state = State.ROTATE_RIGHT;
                    } else if (leftSensor == rightSensor) {
                        if (lastRotate == "right") {
                            state = State.ROTATE_LEFT;
                        } else {
                            state = State.ROTATE_RIGHT;
                        }
                    }
                } else
                    robot.driveMotors(0.15, 0.15);

                if (robot.getReturningLed() == false && beacon.beaconVisible) {
                    state = State.GO_BEACON;
                }
                break;

            case GO_BEACON:
                //System.out.println("GO_BEACON");
                robot.driveMotors(0.08, 0.08);

                if (beacon.beaconDir > -2 && beacon.beaconDir < 2) {
                    robot.driveMotors(0.15, 0.15);
                } else if (beacon.beaconDir > 88 && beacon.beaconDir < 92) {
                    state = State.ROTATE_LEFT;
                } else if (beacon.beaconDir > -92 && beacon.beaconDir < -88) {
                    state = State.ROTATE_RIGHT;
                } else if (beacon.beaconDir > 178 || beacon.beaconDir < -178) {
                    state = State.ROTATE_RIGHT;
                }

                if (robot.getVisitingLed() == false && ground == 0) {/* Visit Target */
                    robot.setVisitingLed(true);
                    state = State.FOUND_CHEESE;
                }
                break;


            case ROTATE_RIGHT:
                //System.out.println("ROTATE_RIGHT");
                if (startRotateTime == 0) {
                    startRotateTime = currentTime;
                }

                if (currentTime - startRotateTime < Time_To_Rotate) {
                    robot.driveMotors(+0.131, -0.131);
                    //System.out.println("Rotating " + compass);
                } else {
                    //System.out.println("ROTATE FINISH" + compass);
                    robot.driveMotors(0.0, 0.0);
                    startRotateTime = 0.0;
                    lastRotate = "right";
                    state = State.MOVE_AVOID;
                }
                break;

            case ROTATE_LEFT:
                //System.out.println("ROTATE_LEFT");
                if (startRotateTime == 0) {
                    startRotateTime = currentTime;
                }

                if (currentTime - startRotateTime < Time_To_Rotate) {
                    robot.driveMotors(-0.131, +0.131);
                    //System.out.println("Rotating " + compass);
                } else {
                    //System.out.println("ROTATE FINISH" + compass);
                    robot.driveMotors(0.0, 0.0);
                    startRotateTime = 0.0;
                    lastRotate = "left";
                    state = State.MOVE_AVOID;
                }
                break;

            case FOUND_CHEESE:
                //System.out.println("Here is the damn CHEESE!!!");
                stop();
                state = State.GO_HOME;
                break;

            case GO_HOME:

                // RESOLVER ISTO, RODAR 180 GRAUS e mudar state para MOVE_AVOID
                //System.out.println("GO_HOME");
                stop();
                robot.setReturningLed(true);

                break;
        }
    }
}
